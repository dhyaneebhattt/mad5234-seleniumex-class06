package class06;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class seleniumex {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public void testSingleInputField() throws InterruptedException
	{

		System.setProperty("webdriver.chrome.driver","/Users/macstudent/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		//tell selenium what page to test
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");

		
		/*stepssss
			1. FIND the textbox.(id="user-message")
			2. type "sdf" in text box(SendKeys('...'))
			3. find the btn(cssSelectors = form#get-input button)
			4, click on btn(.click())
			5. find output msg (id="display")
			6. check output message (expected result = "sdf")
			0. (.getText())
		*/
		
		WebElement textBox = driver.findElement(By.id("user-message"));
		textBox.sendKeys("Hello World!");
		
		WebElement button = driver.findElement(By.cssSelector("form#get-input button"));
		button.click();
		
		WebElement outputSpan = driver.findElement(By.id("display"));
		String outputMessage = outputSpan.getText() ; //actual output
	
		//assertEquals("Hello World!",outputMessage);
		
		//before closing, pause!
		Thread.sleep(2000);
		
		
		
		//at the end close the browser!
		driver.close();
	}

}
